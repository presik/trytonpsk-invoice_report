# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from operator import attrgetter

import polars as pl
from trytond.model import ModelView, fields
from trytond.pool import Pool
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateView, Wizard


class PortfolioDetailedStart(ModelView):
    "Portfolio Detailed Start"
    __name__ = 'invoice_report.portfolio_detailed.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    to_date = fields.Date('To Date')
    parties = fields.Many2Many('party.party', None, None, 'Parties')
    type = fields.Selection([
            ('both', 'Both'),
            ('out', 'Customer'),
            ('in', 'Supplier'),
        ], 'Type', required=True)
    grouped = fields.Selection([
        ("", "")], 'Grouped',
        help="grouped by agent or salesman")
    grouped_party = fields.Boolean('Grouped Party')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_type():
        return 'out'


class PortfolioDetailed(Wizard):
    "Portfolio Detailed"
    __name__ = 'invoice_report.portfolio_detailed'
    start = StateView('invoice_report.portfolio_detailed.start',
        'invoice_report.print_portfolio_detailed_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('party.portfolio_detailed.report')

    def do_print_(self, action):
        parties_ids = [p.id for p in self.start.parties]
        data = {
            'company': self.start.company.id,
            'type': self.start.type,
            'to_date': self.start.to_date,
            'parties': parties_ids,
            'grouped': self.start.grouped,
            'grouped_party': self.start.grouped_party,
            'sellers': None,
            'agents': None,
        }
        if hasattr(self.start, 'sellers') and self.start.sellers:
            data['sellers'] = [s.id for s in self.start.sellers]
        if hasattr(self.start, 'agents') and self.start.agents:
            data['agents'] = [s.id for s in self.start.agents]

        return action, data

    def transition_print_(self):
        return 'end'


class PortfolioDetailedReport(Report):
    __name__ = 'party.portfolio_detailed.report'

    @classmethod
    def get_domain_inv(cls, dom_invoices, data):
        states = ['posted']
        if data['to_date']:
            states.append('paid')
        dom_invoices.append([
            ('company', '=', data['company']),
            ('state', 'in', states),
        ])

        if data['parties']:
            dom_invoices.append(('party', 'in', data['parties']))

        if data['type'] in ['in', 'out']:
            dom_invoices.append(('type', '=', data['type']))

        if data['to_date']:
            dom_invoices.append(('invoice_date', '<=', data['to_date']))
        if data.get('sellers'):
            dom_invoices.append(('salesman', 'in', data['sellers']))
        if data.get('agents'):
            dom_invoices.append(('agent', 'in', data['agents']))
        return dom_invoices

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        cursor = Transaction().connection.cursor()
        pool = Pool()
        Invoice = pool.get('account.invoice')
        dom_invoices = []
        dom_invoices = cls.get_domain_inv(dom_invoices, data)
        invoices = Invoice.search(dom_invoices,
            order=[('party.name', 'ASC'), ('invoice_date', 'ASC')],
        )
        agents = {}
        attrs = attrgetter(*[
            'party', 'payment_lines', 'move', 'lines_to_pay',
            'total_amount', 'amount_to_pay', 'type'])
        for invoice in invoices:
            party, payment_lines, move, lines_to_pay, total_amount, amount_to_pay, type_ = attrs(invoice)
            pay_to_date = []
            if data['to_date']:
                if not move:
                    continue

                move_lines_paid = []
                for line in payment_lines:
                    if line.move.date <= data['to_date']:
                        pay_to_date.append(line.debit - line.credit)
                        move_lines_paid.append(line.id)
                for line in lines_to_pay:
                    if not line.reconciliation:
                        continue
                    for recline in line.reconciliation.lines:
                        if recline.id == line.id or line.id in move_lines_paid:
                            continue
                        if recline.move.date <= data['to_date']:
                            pay_to_date.append(recline.debit - recline.credit)

                amount = sum(pay_to_date)
                if type_ == 'out':
                    amount *= -1
                if amount >= total_amount:
                    continue
                amount_to_pay = total_amount - amount
                amount_to_pay = amount_to_pay
            else:
                amount_to_pay = amount_to_pay

            agent_name = ''
            agent_id = '0'
            party_id = party.id
            if data.get('grouped', None):
                agent = getattr(invoice, data['grouped'])
                agent_name = getattr(agent, 'rec_name', '')
                agent_id = getattr(agent, 'id', '0')

            if agent_id not in agents:
                agents[agent_id] = {
                    'agent': agent_name,
                    'parties': {},
                    'total_invoices': [],
                    'total_amount_to_pay': [],
                    'invoices': [],
                    'opening_balance': [],
                }

            if data['grouped_party']:
                if party_id not in agents[agent_id]['parties']:
                    agents[agent_id]['parties'][party_id] = {
                        'party': invoice.party,
                        'invoices': [],
                        'opening_balance': [],
                        'total_invoices': [],
                        'total_amount_to_pay': [],
                    }
                agents[agent_id]['parties'][party_id]['invoices'].append(invoice)
                agents[agent_id]['parties'][party_id]['total_invoices'].append(total_amount)
                agents[agent_id]['parties'][party_id]['total_amount_to_pay'].append(amount_to_pay)
            else:
                agents[agent_id]['invoices'].append(invoice)

            agents[agent_id]['total_invoices'].append(total_amount)
            agents[agent_id]['total_amount_to_pay'].append(amount_to_pay)

        # openinig_balance
        df = pl.DataFrame()
        if not data.get('agents') and not data.get('sellers'):
            parties = ""
            if data['parties']:
                parties = f"and ml.party in ({','.join(map(str, data['parties']))})"

            date_ = ''
            if data['to_date']:
                date_ = 'and am.date <= {}'.format(data['to_date'])

            type_ = "and at.receivable='t'"
            if data['type'] == 'in':
                type_ = "and at.payable='t'"

            schema_ = {
                'id': pl.Int64,
                'move': pl.Int64,
                'party': pl.Int64,
                'name': pl.String,
                'id_number': pl.String,
                'category': pl.String,
                'reference': pl.String,
                'description': pl.String,
                'date': pl.Date,
                'maturity_date': pl.Date,
                'expired_days': pl.Int64,
                'amount': pl.Float32,
                'payment_amount': pl.Float32,
                'total': pl.Float32,
            }
            query_opening_collection = f"""
                WITH
                    party_category_rel AS (SELECT DISTINCT ON (party) party, category FROM party_category_rel)
                SELECT ml.id, ml.move, pp.id as party, pp.name, pp.id_number, pc.name AS category,
                    ml.reference, ml.description, am.date, ml.maturity_date,
                    (current_date - ml.maturity_date :: date) AS expired_days,
                    (ml.debit - ml.credit) AS amount,
                    COALESCE(sum(av.amount),0) AS payment_amount,
                    ((ml.debit - ml.credit) - COALESCE(sum(av.amount),0)) AS total
                FROM
                    account_move_line AS ml
                    LEFT JOIN account_account AS ac ON ml.account = ac.id
                    LEFT JOIN account_account_type AS at ON ac.type = at.id
                    LEFT JOIN account_voucher_line AS av ON ml.id = av.move_line
                    LEFT JOIN account_move AS am ON am.id = ml.move
                    LEFT JOIN party_party AS pp ON pp.id = ml.party
                    LEFT JOIN party_category_rel AS pcr ON pcr.party = pp.id
                    LEFT JOIN party_category AS pc ON pcr.category = pc.id
                WHERE
                    ac.reconcile='t'
                    {parties}
                    {type_}
                    {date_}
                    AND ml.maturity_date IS NOT NULL
                    AND (am.origin IS NULL or am.origin like 'account.note%')
                    AND ml.reconciliation IS NULL
                GROUP BY ml.id, ml.move, pp.id, pp.name, pp.id_number, pc.name,
                    ml.reference, ml.description, am.date, ml.maturity_date,
                    expired_days, ml.debit, ml.credit;
            """
            df = pl.read_database(query_opening_collection, cursor, schema_overrides=schema_)
            df = df.filter(pl.col('total') > 0)
            agent_id = '0'
            agent_name = ''
            for row in df.to_dicts():
                party_id = row['party']

                if agent_id not in agents:
                    agents[agent_id] = {
                        'agent': agent_name,
                        'parties': {},
                        'total_invoices': [],
                        'total_amount_to_pay': [],
                        'invoices': [],
                        'opening_balance': [],
                    }

                if data['grouped_party']:
                    if party_id not in agents[agent_id]['parties']:
                        agents[agent_id]['parties'][party_id] = {
                            'party': {'name': row['name'], 'id_number': row['id_number']},
                            'invoices': [],
                            'opening_balance': [],
                            'total_invoices': [],
                            'total_amount_to_pay': [],
                        }
                    agents[agent_id]['parties'][party_id]['opening_balance'].append(row)
                    agents[agent_id]['parties'][party_id]['total_invoices'].append(Decimal(str(row['amount'])))
                    agents[agent_id]['parties'][party_id]['total_amount_to_pay'].append(Decimal(str(row['total'])))
                else:
                    agents[agent_id]['opening_balance'].append(row)
                agents[agent_id]['total_invoices'].append(Decimal(str(row['amount'])))
                agents[agent_id]['total_amount_to_pay'].append(Decimal(str(row['total'])))

        report_context['records'] = agents
        report_context['data'] = data
        return report_context
